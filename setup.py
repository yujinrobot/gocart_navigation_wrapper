#!/usr/bin/env python

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['gocart_navigation_wrapper'],
    scripts=[
        'nodes/run_wrapper.py',
        'nodes/run_patrol.py',
    ],
)

setup(**d)
