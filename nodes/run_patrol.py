#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# License: Yujin Robot

from __future__ import absolute_import, division, print_function

import rospy
import threading

import geometry_msgs.msg as geometry_msgs

from gocart_navigation_wrapper import init_pose
from gocart_navigation_wrapper import patrol


# The name of current map where robot is located
map_name = 'yujin_floor3_0529'

if __name__ == '__main__':
    rospy.init_node('patrol_test')
    init_pose.reset_pose()  # Before initialisastion, you must need to reset pose
    # Set robot pose in the map as (x=0.0, y=0.0, theta=0.0)
    init_pose.initialise(map_name, pose2d=geometry_msgs.Pose2D(0.0, 0.0, 0.0))
    # Set patrol points
    ptrl_point1 = patrol.PatrolPoint(target_pose=geometry_msgs.Pose2D(1.0, 0.0, 0.0))
    ptrl_point2 = patrol.PatrolPoint(target_pose=geometry_msgs.Pose2D(0.0, 0.0, 3.14))
    ptrl_point3 = patrol.PatrolPoint(target_pose=geometry_msgs.Pose2D(0.0, 1.0, 1.57))
    ptrl_point4 = patrol.PatrolPoint(target_pose=geometry_msgs.Pose2D(0.0, 0.0, -1.57))
    ptrl_points = [ptrl_point1, ptrl_point2, ptrl_point3, ptrl_point4]
    # Create Patrol object
    ptrl = patrol.Patrol(ptrl_points)
    rospy.on_shutdown(ptrl.stop)
    # run patrol
    ptrl_thread = threading.Thread(target=ptrl.run, args=(1, ))
    ptrl_thread.start()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        ptrl.stop()
        sys.exit(0)
