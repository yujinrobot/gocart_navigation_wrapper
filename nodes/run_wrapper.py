#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# License: Yujin Robot

from __future__ import absolute_import, division, print_function

import rospy

import geometry_msgs.msg as geometry_msgs

from gocart_navigation_wrapper import init_pose
from gocart_navigation_wrapper import move_base_wrapper


def mbw_status_cb(status_change, msg):
    print("Status callback: {} / {}".format(status_change, msg))

# The name of current map where robot is located
map_name = 'yujin_floor3_0529'

if __name__ == '__main__':
    rospy.init_node('navi_wrapper_test')
    init_pose.reset_pose()  # Before initialisastion, you must need to reset pose
    # Set robot pose in the map as (x=0.0, y=0.0, theta=0.0)
    init_pose.initialise(map_name, pose2d=geometry_msgs.Pose2D(0.0, 0.0, 0.0))
    # wrapper object, set target pose
    mbw = move_base_wrapper.MoveBaseWrapper(
        target_pose=geometry_msgs.Pose2D(2.0, 0.0, 1.57), status_cb=mbw_status_cb)
    try:
        # robot will go to point (x=2.0, y=0.0, theta=1.57(1/2pi))
        mbw.send_goal()
    except Exception as exc:
        rospy.logerr(str(exc))
    rospy.spin()
