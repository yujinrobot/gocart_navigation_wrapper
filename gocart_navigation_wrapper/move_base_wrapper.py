#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# License: Yujin Robot

from __future__ import absolute_import, division, print_function

import actionlib
import os
import rospy
import threading

import gopher_configuration
import gopher_semantics
import gopher_siteconf

import actionlib_msgs.msg as actionlib_msgs
import geometry_msgs.msg as geometry_msgs
import nav_msgs.msg as nav_msgs

import gopher_navi_msgs.msg as gopher_navi_msgs
import gopher_std_msgs.msg as gopher_std_msgs
import gopher_std_msgs.srv as gopher_std_srvs
import move_base_msgs.msg as move_base_msgs

from . import transform_utilities


"""

*********************************
# GoalFinishing.action definition
*********************************

#goal

bool no_alignment
bool align_on_failure
geometry_msgs/PoseStamped pose
---

# result

string  text
float64 goal_distance

int32 SUCCESS           =  0
int32 FAIL              = -1
int32 FAIL_TO_ALIGN     = -2

int32 value
---

# feedback
string status_info


****************************
# MoveBase.action definition
****************************

# goal
geometry_msgs/PoseStamped target_pose
---

# result

---
# feedback
geometry_msgs/PoseStamped base_position

"""


class ActionsStatus(object):
    MOVE_BASE_START = 1
    MOVE_BASE_DONE = 2
    FINISHER_START = 3
    FINISHER_DONE = 4


class MoveBaseWrapper(object):

    def __init__(self, target_location=None, target_pose=geometry_msgs.Pose2D(),
                 goal_finishing=True, status_cb=None):
        self.target_location = target_location
        self.target_pose = target_pose
        self.goal_finishing = goal_finishing
        # Flags
        self.is_goal_ready = False
        self.goal_sent = False
        # Action clients
        self._move_base_client = actionlib.SimpleActionClient(
            '/navi/move_base', move_base_msgs.MoveBaseAction)
        self._finisher_client = actionlib.SimpleActionClient(
            '/core/goal_finisher', gopher_navi_msgs.GoalFinishingAction)
        # Action goals
        self._move_base_goal = move_base_msgs.MoveBaseGoal()
        self._finisher_goal = gopher_navi_msgs.GoalFinishingGoal()
        # Goal lock
        self._goal_lock = threading.Lock()
        # Subscribe current robot pose/odom
        self._cur_robot_pose = geometry_msgs.PoseWithCovarianceStamped()
        self._cur_robot_odom = nav_msgs.Odometry()
        self._pose_subscriber = rospy.Subscriber(
            '/navi/pose', geometry_msgs.PoseWithCovarianceStamped, self._pose_callback)
        self._odom_subscriber = rospy.Subscriber(
            '/core/odom', nav_msgs.Odometry, self._odom_callback)
        # status change callback
        self._status_cb = status_cb
        """
        status callback prototype
        def some_function(status_change, msg):
            # status_change (int type)
                - MOVE_BASE_START = 1
                - MOVE_BASE_DONE = 2
                - FINISHER_START = 3
                - FINISHER_DONE = 4
            # msg:
                string type
        """
        # goal setup
        self.setup_goal()

    def set_status_cb(self, status_cb):
        self._status_cb = status_cb

    def setup_goal(self):
        try:
            self._setup_goal()
        except Exception as e:
            rospy.logerr(str(e))
            self.is_goal_ready = False

    def change_goal(self, target_location=None, target_pose=geometry_msgs.Pose2D(), goal_finishing=True):
        self.target_location = target_location
        self.target_pose = target_pose
        self.goal_finishing = goal_finishing
        self.setup_goal()

    def send_goal(self):
        with self._goal_lock:
            if not self.is_goal_ready:
                raise Exception("Move-base goal is not ready")
            if not self._move_base_client.wait_for_server(rospy.Duration(1.0)):
                raise Exception("Cannot connect to move-base action server")
            if self.goal_sent:  # if there are already running goals, cancel them
                self.cancel_goal()
            # send a new move_base goal
            self._move_base_client.send_goal(
                self._move_base_goal,
                done_cb=self._move_base_done_cb,
                feedback_cb=self._move_base_feedback_cb)
            self.goal_sent = True
            self._on_status_change(
                ActionsStatus.MOVE_BASE_START,
                "Move-base goal sent: {}".format(self._move_base_goal))

    def cancel_goal(self):
        self._move_base_client.cancel_all_goals()
        self._finisher_client.cancel_all_goals()

    def _move_base_done_cb(self, status, result):
        self._on_status_change(
            ActionsStatus.MOVE_BASE_DONE,
            "Move-base summary: status({}) / result({})".format(status, result))
        if not self._finisher_client.wait_for_server(rospy.Duration(1.0)):
            # move_base is done, but cannot do goal finishing
            rospy.logwarn("Cannot connect to goal-finisher action server")
            self.goal_sent = False
            return
        if self.goal_finishing:
            # send a new goal_finishing goal to place robot in the correct position
            self._finisher_client.send_goal(
                self._finisher_goal,
                done_cb=self._finisher_done_cb,
                feedback_cb=self._finisher_feedback_cb)
            self._on_status_change(
                ActionsStatus.FINISHER_START,
                "Goal-finisher goal sent: {}".format(self._finisher_goal))

    def _move_base_feedback_cb(self, feedback):
        rospy.logdebug("Move-base feedback: " + str(feedback))

    def _finisher_done_cb(self, status, result):
        """
        result value:
            int32 SUCCESS           =  0
            int32 FAIL              = -1
            int32 FAIL_TO_ALIGN     = -2
        """
        if result.value == gopher_navi_msgs.GoalFinishingResult.SUCCESS:
            result_value_text = "Success"
        elif result.value == gopher_navi_msgs.GoalFinishingResult.FAIL:
            result_value_text = "Fail"
        elif result.value == gopher_navi_msgs.GoalFinishingResult.FAIL_TO_ALIGN:
            result_value_text = "Fail to align"
        self._on_status_change(
            ActionsStatus.FINISHER_DONE,
            "Goal-finisher summary:\n"
            "status: {}\n"
            "result: {}\n"
            "message: {}\n"
            "goal-distance: {}".format(status, result_value_text, result.text, result.goal_distance))
        self.goal_sent = False

    def _finisher_feedback_cb(self, feedback):
        rospy.logdebug("Goal-finisher feedback: " + str(feedback))

    def _pose_callback(self, pose_data):
        self._cur_robot_pose = pose_data

    def _odom_callback(self, odom_data):
        self._cur_robot_odom = odom_data

    def _setup_goal(self):
        self._move_base_goal.target_pose.header.frame_id = "map"  # not important, just don't leave it as empty.
        if self.target_location:
            gopher = gopher_configuration.configuration.Configuration(fallback_to_defaults=True)
            # this if statement is to cover all versions of the gopher software semantics
            if 'GOPHER_SITECONF' in os.environ:
                semantics = gopher_siteconf.Semantics(gopher.namespaces.semantics)
            elif 'GOPHER_SEMANTICS' in os.environ:
                semantics = gopher_semantics.Semantics(gopher.namespaces.semantics)
            else:
                raise Exception("Semantics file path is not set")
            location = semantics.locations[self.target_location]
            target_pose2d = location.pose  # type: geometry_msgs.Pose2D
        else:
            target_pose2d = self.target_pose
        # move_base_goal
        self._move_base_goal.target_pose.pose = transform_utilities.convert_pose2d_to_pose(target_pose2d)
        # finisher_goal
        self._finisher_goal.pose.header.stamp = rospy.get_rostime()
        self._finisher_goal.pose.header.frame_id = 'odom'
        pose_map_rel_base = transform_utilities.get_inverse_pose(self._cur_robot_pose.pose.pose)
        pose_map_rel_odom = transform_utilities.concatenate_poses(pose_map_rel_base, self._cur_robot_odom.pose.pose)
        pose_goal_rel_odom = \
            transform_utilities.concatenate_poses(self._move_base_goal.target_pose.pose, pose_map_rel_odom)
        self._finisher_goal.pose.pose = pose_goal_rel_odom
        self._finisher_goal.align_on_failure = False
        self._finisher_goal.no_alignment = False
        # is ready?
        self.is_goal_ready = True

    def _on_status_change(self, status_change, msg):
        if self._status_cb:
            self._status_cb(status_change, msg)
        rospy.loginfo(msg)
