#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import math
import numpy

import tf.transformations

import geometry_msgs.msg as geometry_msgs


def transform_from_geometry_msgs_pose(pose):
    """
    Convert a geometry_msgs/Pose object into a numpy homogenous 4x4 matrix.

    :param geometry_msgs/Pose pose: incoming pose
    :returns: numpy homogenous matrix representing the transform (inverse of the pose matrix)

    .. seealso:: `Transform Cheat Sheet`_

    .. _Transform Cheat Sheet: https://docs.google.com/document/d/1HRgc0avn08vL_LGN8-SuiaaSQUYt4HbBwwZtVOGzMIo/edit
    """
    translation = [pose.position.x, pose.position.y, pose.position.z]
    translation_homogenous_matrix = tf.transformations.translation_matrix(translation)
    quaternion = [pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w]
    orientation_homogenous_matrix = tf.transformations.quaternion_matrix(quaternion)
    homogeneous_matrix = numpy.dot(translation_homogenous_matrix, orientation_homogenous_matrix)
    # remember transform is the inverse of pose
    return tf.transformations.inverse_matrix(homogeneous_matrix)


def get_inverse_pose(pose_a_rel_b):
    """
    Computes the inverse pose and returns the geometry_msgs object.

    :param geometry_msgs/Pose pose_a_rel_b:
    :returns: pose_b_rel_a
    :rtype: geometry_msgs/Pose
    """
    pose_b_rel_a = transform_from_geometry_msgs_pose(pose_a_rel_b)
    # lists
    quaternion = tf.transformations.quaternion_from_matrix(pose_b_rel_a)
    translation = tf.transformations.translation_from_matrix(pose_b_rel_a)
    geometry_msgs_pose_b_rel_a = geometry_msgs.Pose()
    geometry_msgs_pose_b_rel_a.position = geometry_msgs.Point(translation[0], translation[1], translation[2])
    geometry_msgs_pose_b_rel_a.orientation = \
        geometry_msgs.Quaternion(x=quaternion[0], y=quaternion[1], z=quaternion[2], w=quaternion[3])
    return geometry_msgs_pose_b_rel_a


def concatenate_poses(pose_a_rel_b, pose_b_rel_c):
    """
    Generate pose_a_rel_c given pose_a_rel_b and pose_b_rel_c
    """
    T_a_rel_b = transform_from_geometry_msgs_pose(pose_a_rel_b)
    T_b_rel_c = transform_from_geometry_msgs_pose(pose_b_rel_c)
    T_a_rel_c = tf.transformations.concatenate_matrices(T_a_rel_b, T_b_rel_c)
    pose_a_rel_c = tf.transformations.inverse_matrix(T_a_rel_c)
    # lists
    quaternion = tf.transformations.quaternion_from_matrix(pose_a_rel_c)
    translation = tf.transformations.translation_from_matrix(pose_a_rel_c)
    # convert to geometry_msgs.Pose
    geometry_msgs_pose_pose_a_rel_c = geometry_msgs.Pose()
    geometry_msgs_pose_pose_a_rel_c.position = geometry_msgs.Point(translation[0], translation[1], translation[2])
    geometry_msgs_pose_pose_a_rel_c.orientation = \
        geometry_msgs.Quaternion(x=quaternion[0], y=quaternion[1], z=quaternion[2], w=quaternion[3])
    return geometry_msgs_pose_pose_a_rel_c


def convert_pose2d_to_pose(pose2d):
    pose = geometry_msgs.Pose()
    pose.position.x = pose2d.x
    pose.position.y = pose2d.y
    pose.position.z = 0.0
    orientation_quat = tf.transformations.quaternion_from_euler(0, 0, pose2d.theta)
    pose.orientation.x = orientation_quat[0]
    pose.orientation.y = orientation_quat[1]
    pose.orientation.z = orientation_quat[2]
    pose.orientation.w = orientation_quat[3]
    return pose
