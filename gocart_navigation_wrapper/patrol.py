#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# License: Yujin Robot

from __future__ import absolute_import, division, print_function

import rospy

import geometry_msgs.msg as geometry_msgs

from . import move_base_wrapper


class PatrolPoint(object):

    def __init__(self, target_location=None, target_pose=geometry_msgs.Pose2D(), goal_finishing=True):
        self.target_location = target_location
        self.target_pose = target_pose
        self.goal_finishing = goal_finishing


class Patrol(object):

    running = 0

    def __init__(self, patrol_points):
        self._mbws = []
        self._cur_idx = 0
        self._next = False
        self._repeat = 0
        self._cancel_requested = False
        self._setup(patrol_points)

    def set_patrol_points(self, patrol_points):
        self._setup(patrol_points)

    def run(self, repeat=-1):
        Patrol.running += 1  # acquire
        while Patrol.running > 1:
            # preempting.. wait until previous use is released
            rospy.sleep(1)
        self._repeat = repeat
        while self._repeat != 0:
            for idx, mbw in enumerate(self._mbws):
                self._cur_idx = idx
                mbw.send_goal()
                while not self._next \
                        and Patrol.running == 1 and not self._cancel_requested:
                    # wait until the mbw goal is achieved
                    rospy.sleep(1)
                if Patrol.running > 1 or self._cancel_requested:
                    # preempted.. break all loops
                    self._repeat = 0
                    mbw.cancel_goal()
                    break
            if self._repeat > 0:
                self._repeat -= 1
        self._cur_idx = 0
        self._next = False
        self._cancel_requested = False
        Patrol.running -= 1  # release

    def stop(self):
        if self._mbws:
            self._mbws[self._cur_idx].cancel_goal()
        del self._mbws[:]
        self._cur_idx = 0
        self._next = False
        self._repeat = 0
        self._cancel_requested = True

    def _setup(self, patrol_points):
        if self._mbws:
            del self._mbws[:]
        for patrol_point in patrol_points:
            mbw = move_base_wrapper.MoveBaseWrapper(
                target_location=patrol_point.target_location,
                target_pose=patrol_point.target_pose,
                goal_finishing=patrol_point.goal_finishing,
                status_cb=self._on_status_change)
            self._mbws.append(mbw)

    def _on_status_change(self, status_change, msg):
        cur_mbw = self._mbws[self._cur_idx]
        if status_change == move_base_wrapper.ActionsStatus.MOVE_BASE_DONE and \
                cur_mbw.goal_finishing is False:
            self._next = True
        elif status_change == move_base_wrapper.ActionsStatus.FINISHER_DONE:
            self._next = True
        else:
            self._next = False
