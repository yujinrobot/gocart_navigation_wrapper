#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# License: Yujin Robot

from __future__ import absolute_import, division, print_function

import actionlib
import rospy
import tf

import geometry_msgs.msg as geometry_msgs
import std_msgs.msg as std_msgs

import gopher_navi_msgs.msg as gopher_navi_msgs

from . import transform_utilities


pose_resetter = rospy.Publisher(
    '/navi/elf/reset', std_msgs.Empty, queue_size=1)

pose_setter = \
    actionlib.SimpleActionClient(
        '/navi/teleport', gopher_navi_msgs.TeleportAction)


def reset_pose():
    global pose_resetter
    pose_resetter.publish(std_msgs.Empty())


def initialise(world, pose2d=geometry_msgs.Pose2D()):
    global pose_setter
    # Pose transformed
    _pose = geometry_msgs.PoseWithCovarianceStamped()
    _pose.pose.pose = transform_utilities.convert_pose2d_to_pose(pose2d)
    # Set world pose
    world_pose = gopher_navi_msgs.WorldPoseLocation(world=world, pose=_pose)
    goal = gopher_navi_msgs.TeleportGoal(world_pose=world_pose, special_effects=True)
    # send goal and wait
    pose_setter.send_goal_and_wait(goal)
